FROM fedora:31

LABEL \
    maintainer="Syseleven MetaOps Team <metaops@syseleven.de>" \
    org.opencontainers.image.authors="Syseleven MetaOps Team <metaops@syseleven.de>" \
    org.opencontainers.image.description="Skopeo in a container for gitlab-ci" \
    org.opencontainers.image.title="Skopeo" \
    org.opencontainers.image.version="0.1.41" \
    org.opencontainers.image.url="https://gitlab.com/syseleven/container-images/skopeo" \
    org.opencontainers.image.documentation="https://gitlab.com/syseleven/container-images/skopeo/-/blob/master/README.md" \
    org.opencontainers.image.source="https://github.com/containers/skopeo" \
    org.opencontainers.image.revision="https://github.com/containers/skopeo/tree/v0.1.41" \
    org.opencontainers.image.licenses="Apache-2.0" \
    org.opencontainers.image.vendor="SysEleven GmbH"

RUN dnf -y update \
    && dnf install -y skopeo \
    && dnf clean all

ENTRYPOINT ["skopeo"]
